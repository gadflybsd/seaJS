/**
 * Created by GadflyBSD on 14-6-5.
 */
define(function(require, exports, module){
	var jQuery = require('jquery');
	(function ($) {
		jQuery.fn.tagsHandler = function(options){
			var async = ['tagHandler'], $this = $(this);
			if(options.autocomplete) async.push('ui/jquery.ui.menu.min', 'ui/jquery.ui.autocomplete.min');
			require.async(async, function(){
				$this.tagHandler(options);
			})
		}
	})(jQuery);
	return jQuery;
});