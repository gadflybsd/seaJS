define(function(require, exports, module){
	var jQuery = require('jquery');
	require('Gallery/baoshanSlider/style.css');
	(function ($) {
		$.fn.baoshanSlider = function(){
			$('.silder_nav>li', $(this)).removeClass('silder_panel clearfix');
			var $this = $(this),
				sWidth = $this.width(),
				len = $(".silder_panel", $this).length,
				index = 0, picTimer,
				btn = "<a class='prev'>Prev</a><a class='next'>Next</a>";
			$this.append(btn);
			$(".silder_nav li", $this).css({"opacity":"0.6","filter":"alpha(opacity=60)"}).mouseenter(function() {
				index = $(".silder_nav li", $this).index(this);
				showPics(index);
			}).eq(0).trigger("mouseenter");
			$(".prev,.next", $this).css({"opacity":"0.2","filter":"alpha(opacity=20)"}).hover(function(){
				$(this).stop(true,false).animate({"opacity":"0.6","filter":"alpha(opacity=60)"},300);
			},function() {
				$(this).stop(true,false).animate({"opacity":"0.2","filter":"alpha(opacity=20)"},300);
			});
			// Prev
			$(".prev", $this).click(function() {
				index -= 1;
				if(index == -1) {index = len - 1;}
				showPics(index);
			});
			// Next
			$(".next", $this).click(function() {
				index += 1;
				if(index == len) {index = 0;}
				showPics(index);
			});
			$(".silder_con", $this).css("width",sWidth * (len));
			alert(sWidth+'-'+len);
			// mouse
			$this.hover(function() {
				clearInterval(picTimer);
			},function() {
				picTimer = setInterval(function() {
					showPics(index);
					index++;
					if(index == len) {index = 0;}
				},3000);
			}).trigger("mouseleave");
			// showPics
			function showPics(index) {
				var nowLeft = -index*sWidth;
				$(".silder_con", $this).stop(true,false).animate({"left":nowLeft},300);
				$(".silder_nav li", $this).removeClass("current").eq(index).addClass("current");
				$(".silder_nav li", $this).stop(true,false).animate({"opacity":"0.5"},300).eq(index).stop(true,false).animate({"opacity":"1"},300);
			}
		}
	}(jQuery));
	return jQuery;
});