/**
 * Created by GadflyBSD on 14-7-16.
 */
define( function(require, exports, module) {
	var jQuery  = require('jquery');
	(function ($) {
		$.extend($, {
			Messenger: function(message){
				require.async('messenger', function(){
					Messenger().hideAll();
					Messenger().post({
						type: 'success',
						message: message
					});
				});
			},
			DialogComplete: function(SWFupload, numFilesSelected, numFilesQueued, totalNumFilesQueued){
				require.async('messenger', function(){
					var Mess = Messenger().post({
						message: '选择了'+numFilesSelected+'个文件，'+numFilesQueued+'个文件加入上传队列，上传队列中共有'+totalNumFilesQueued+'个文件',
						type: 'error',
						actions: {
							retry: {
								label: '上传',
								phrase: 'Retrying TIME',
								auto: true,
								delay: 10,
								action: function(){
									SWFupload.startUpload();
									Mess.hide()
								}
							},
							cancel: {
								label: "稍后上传",
								action: function(){
									Mess.cancel();
								}
							},
							delete: {
								label: "清除队列",
								action: function(){
									SWFupload.cancelQueue();
									Mess.hide();
								}
							}
						}
					});
				});
			},
			QueueComplete: function(numFilesUploaded){
				var message = numFilesUploaded + " file:" + (numFilesUploaded === 1 ? "" : "s") + " uploaded.";
				$.Messenger(message);
			},
			uploadStart: function(file){
				var message = file.name + '文件正在上传...';
				$.Messenger(message);
			}
		});
		$.extend($.fn, {
			seaSWFupload: function(options){
				var $this = $(this),
					config = {
					flash_url : seajs.data.base+"Upload/swfupload/swfupload.swf",
					flash9_url : seajs.data.base+"Upload/swfupload/swfupload_fp9.swf",
					upload_url: seajs.data.base+"Upload/swfupload/upload.php",
					css: "Upload/uploadify/uploadify.css",
					file_size_limit : "100 MB",
					file_types : "*.*",
					file_types_description : "All Files",
					file_upload_limit : 100,
					file_queue_limit : 0,
					debug: false,
					// Button Settings
					button_image_url : seajs.data.base+"Upload/swfupload/XPButtonNoText_61x22.png",
					button_placeholder_id : "spanButtonPlaceholder",
					button_width: 61,
					button_height: 22,
					button_text: '<span class="theFont">上传图片</span>',
					button_text_style: ".theFont { font-size: 12px; }",
					button_text_left_padding: 5,
					button_text_top_padding: 2,
					// The event handler functions are defined in handlers.js
					queueDom: 'div',
					swfupload_preload_handler : function(){
						if (!this.support.loading) {
							alert("您需要 Flash Player 版本高于 9.028 才能运行 SWFUpload！");
							return false;
						}
					},
					swfupload_load_failed_handler : function(){
						alert("当页面不能正常加载 SWFupload flash。可能是因为没有安装Flash Player或者它的版本低于 9.0.28！");
					},
					swfupload_loaded_handler : function(){},
					file_queued_handler : function(file){
						try {
							$('#tdFilesQueued').text(this.getStats().files_queued);
						} catch (ex) {
							this.debug(ex);
						}
					},
					file_queue_error_handler : function(){},
					file_dialog_complete_handler : function(numFilesSelected, numFilesQueued, totalNumFilesQueued){
						try {
							if (numFilesSelected > 0) {
								$.DialogComplete(this, numFilesSelected, numFilesQueued, totalNumFilesQueued);
							}
						} catch (ex)  {
							this.debug(ex);
						}
					},
					upload_start_handler : function(file) {
						try {
							$.uploadStart(file);
							this.customSettings.progressCount = 0;
							updateDisplay.call(this, file);
						}catch (ex) {
							this.debug(ex);
						}
					},
					upload_progress_handler : function(file, bytesLoaded, bytesTotal){
						//try {
						this.customSettings.progressCount++;
						updateDisplay.call(this, file);
						//} catch (ex) {
						//	this.debug(ex);
						//}
					},
					upload_error_handler : function(){},
					upload_success_handler : function(){},
					upload_complete_handler : function(file) {
						$('#tdFilesQueued').text(this.getStats().files_queued);
						$('#tdFilesUploaded').text(this.getStats().successful_uploads);
						$('#tdErrors').text(this.getStats().upload_errors);
					},
					queue_complete_handler : function(numFilesUploaded) {
						$.QueueComplete(numFilesUploaded);
					}
				};
				$.seaBase.run(config, options, function(setting){
					if(!config.id) config.id = 'seaSWFupload_'+ $.seaBase.mathRand();
					$this.attr('id', config.id);
					config.button_placeholder_id = config.id;
					setting.upload_url = ($this.data('url'))?$this.data('url'):setting.upload_url;
					require.async('Upload/swfupload/swfupload', function(){
						//console.log(setting);
						SWFUpload.onload = function(){setting};
						var swfu = new SWFUpload(setting);
						var appendQueue = '<'+setting.queueDom+' class="'+swfu.movieName+'_Queue" \>';
						$('#'+swfu.movieName).parent('div').append(appendQueue);
						//console.log(swfu);
					});
				});
			}
		});
	}(jQuery));
	return jQuery;
});